<?php

    // Get penerbit
    $queryPenerbit = 'select * from penerbit';

    if(isset($_GET['cariPenerbit'])) {
        $cariPenerbit = $_GET['cariPenerbit'];
        $queryPenerbit .= " where nama like '%{$cariPenerbit}%' or kota like '%{$cariPenerbit}%' or telepon like '%{$cariPenerbit}%'";
    }

    $getPenerbit = DB::query($db, $queryPenerbit);

    if(isset($_GET['actionPenerbit'])) {
        // Create barang
        if($_GET['actionPenerbit'] == 'createPenerbit') {
            // Generate id
            $id = generateIdPenerbit($db);
            $_POST['id'] = $id;
            
            // Store
            DB::store($db, 'penerbit', $_POST);
        }   

        // Update barang
        if($_GET['actionPenerbit'] == 'updatePenerbit') {
            DB::update($db, ['id' => $_GET['id']], 'penerbit', $_POST);
        }

        // Delete barang
        if($_GET['actionPenerbit'] == 'deletePenerbit') {
            DB::delete($db, ['id' => $_GET['id']], 'penerbit');
        }

        echo "<script>alert('Berhasil');</script>";
        echo "<script>location.href = 'admin.php';</script>";
    }

    function generateIdPenerbit($db) {
        $getLatestPenerbit = DB::query($db, "select * from penerbit order by id desc limit 1");
        // Latest number + 1
        $getLatestNumber = substr($getLatestPenerbit->fetch_assoc()['id'], -1) + 1;
        $idFormat = 'SP01';
        $id = '';

        // Generate id
        if($getLatestNumber == null) {
            $id = $idFormat;
        } else {
            // Format example B1---
            $id = 'SP' . str_pad($getLatestNumber, 2, "0", STR_PAD_LEFT);
        }

        return $id;
    }