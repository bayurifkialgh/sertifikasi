<?php

    // Get barang
    $queryBarang = 'select b.*, p.nama as penerbit from buku b join penerbit p on b.penerbit_id = p.id';

    if(isset($_GET['cariBarang'])) {
        $cariBarang = $_GET['cariBarang'];
        $queryBarang .= " where p.nama like '%{$cariBarang}%' or b.kategori like '%{$cariBarang}%' or b.nama like '%{$cariBarang}%' or b.id like '%{$cariBarang}%'";
    }

    $queryBarang .= ' order by b.nama asc';

    $getBarang = DB::query($db, $queryBarang);

    if(isset($_GET['action'])) {
        // Create barang
        if($_GET['action'] == 'createBarang') {
            // Generate id
            $id = generateId($db, $_POST);
            $_POST['id'] = $id;
            
            // Store
            DB::store($db, 'buku', $_POST);
        }   

        // Update barang
        if($_GET['action'] == 'updateBarang') {
            DB::update($db, ['id' => $_GET['id']], 'buku', $_POST);
        }

        // Delete barang
        if($_GET['action'] == 'deleteBarang') {
            DB::delete($db, ['id' => $_GET['id']], 'buku');
        }

        echo "<script>alert('Berhasil');</script>";
        echo "<script>location.href = 'admin.php';</script>";
    }

    function generateId($db, $post) {
        $getLatestBarang = DB::query($db, "select * from buku where kategori = '$post[kategori]' order by id desc limit 1");
        // Latest number + 1
        $getLatestNumber = substr($getLatestBarang->fetch_assoc()['id'], -1) + 1;
        $getFirstLaterOfCategory = substr($post['kategori'], 0, 1);
        $idFormat = $getFirstLaterOfCategory . '1001';
        $id = '';

        // Generate id
        if($getLatestNumber == null) {
            $id = $idFormat;
        } else {
            // Format example B1---
            $id = $getFirstLaterOfCategory . '1' . str_pad($getLatestNumber, 3, "0", STR_PAD_LEFT);
        }

        return $id;
    }