<?php
    $page = $_SERVER['REQUEST_URI'];
    $page = explode('/', $page);
    $page = $page[2] ? $page[2] : 'index.php';
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">
        UNIBOOKSTORE
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= $page == 'index.php' ? 'active' : '' ?>">
                <a class="nav-link" href="index.php">
                    Home
                </a>
            </li>
            <li class="nav-item <?= $page == 'admin.php' ? 'active' : '' ?>">
                <a class="nav-link" href="admin.php">
                    Admin
                </a>
            </li>
            <li class="nav-item <?= $page == 'pengadaan.php' ? 'active' : '' ?>">
                <a class="nav-link" href="pengadaan.php">
                    Pengadaan
                </a>
            </li>
        </ul>
    </div>
</nav>
