<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            <h5>
                Data Barang
            </h5>
        </div>
        <div class="card-body">
            <form class="row mb-3" action="admin.php">
                <div class="col-md-6">
                    <input type="text" class="form-control" name="cariBarang" value="<?= $cariBarang ?? '' ?>">
                </div>
                <div class="col-md-6">
                    <div class="float-right">
                        <a href="admin.php" class="btn btn-warning">
                            Reset
                        </a>
                        <button class="btn btn-success" type="submit">
                            Cari
                        </button>
                    </div>
                </div>
            </form>
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="float-right">
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-create">
                            Tambah
                        </button>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <td>Kode</td>
                            <td>Penerbit</td>
                            <td>Kategori</td>
                            <td>Nama</td>
                            <td>Harga</td>
                            <td>Stok</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($getBarang as $d) :?>
                        <tr>
                            <td><?= $d['id'] ?></td>
                            <td><?= $d['penerbit'] ?></td>
                            <td><?= $d['kategori'] ?></td>
                            <td><?= $d['nama'] ?></td>
                            <td><?= rupiah($d['harga']) ?></td>
                            <td><?= number_format($d['stok']) ?></td>
                            <td>
                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-update<?= $d['id'] ?>">
                                    Edit
                                </button>
                                <a href="admin.php?id=<?= $d['id'] ?>&action=deleteBarang" onclick="return confirm('Yakin?')"
                                    class="btn btn-danger btn-sm">
                                    Delete
                                </a>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?= $getBarang->num_rows == 0 ? '<tr><td colspan="5" class="text-center">Tidak ada data</td></tr>' : '' ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="modal-create-label"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="admin.php?action=createBarang" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-create-label">Buat Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label">Penerbit</label>
                            <select name="penerbit_id" class="form-control" required>
                                <option value="">--Pilih Penerbit--</option>
                                <?php foreach($getPenerbit as $sp): ?>
                                    <option value="<?= $sp['id'] ?>"><?= $sp['nama'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Kategori</label>
                            <select name="kategori" class="form-control" required>
                                <option value="">--Pilih Kategori--</option>
                                <option value="Bisnis">Bisnis</option>
                                <option value="Keilmuan">Keilmuan</option>
                                <option value="Novel">Novel</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" required> 
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Harga</label>
                            <input type="number" class="form-control" name="harga" required> 
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Stok</label>
                            <input type="number" class="form-control" name="stok" required> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php foreach($getBarang as $upd) :?>
<div class="modal fade" id="modal-update<?= $upd['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modal-update-label<?= $upd['id'] ?>"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="admin.php?action=updateBarang&id=<?= $upd['id'] ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-update-label<?= $upd['id'] ?>">Update Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label">Penerbit</label>
                            <select name="penerbit_id" class="form-control" required>
                                <option value="">--Pilih Penerbit--</option>
                                <?php foreach($getPenerbit as $sp): ?>
                                    <option value="<?= $sp['id'] ?>" <?= $upd['penerbit_id'] == $sp['id'] ? 'selected' : '' ?>><?= $sp['nama'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Kategori</label>
                            <select name="kategori" class="form-control" required>
                                <option value="">--Pilih Kategori--</option>
                                <option value="Bisnis" <?= $upd['kategori'] == 'Bisnis' ? 'selected' : '' ?>>Bisnis</option>
                                <option value="Keilmuan" <?= $upd['kategori'] == 'Keilmuan' ? 'selected' : '' ?>>Keilmuan</option>
                                <option value="Novel" <?= $upd['kategori'] == 'Novel' ? 'selected' : '' ?>>Novel</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" required value="<?= $upd['nama'] ?>"> 
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Harga</label>
                            <input type="number" class="form-control" name="harga" required value="<?= $upd['harga'] ?>"> 
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Stok</label>
                            <input type="number" class="form-control" name="stok" required value="<?= $upd['stok'] ?>">  
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php endforeach ?>
