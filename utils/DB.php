<?php

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', 'secretpassword');
define('DB_NAME', 'sertifikasi');

class DB
{
    public static function connect() {
        $connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if (!$connection) {
            die("Connection failed: " . mysqli_connect_error());
        }
        return $connection;
    }

    public static function close($connection) {
        mysqli_close($connection);
    }

    public static function query($connection, $query) {
        $result = mysqli_query($connection, $query);
        if (!$result) {
            die("Query failed: " . mysqli_error($connection));
        }
        return $result;
    }
    
    public static function store($connection, $tabel,$data)
    {
        $r1 = array();
        $f1 = array();

        foreach($data as $f2=>$r2)
        {
            array_push($f1,$f2);
            array_push($r1,"'".$r2."'");
        }

        $field = implode(',',$f1);
        $row = implode(',',$r1);

        $query = $connection->query("INSERT INTO {$tabel}({$field}) VALUES ({$row})");

        return $query;
    }

    public static function update($connection, $tabel_id,$tabel,$data)
    {
        $r1 = array();

        foreach($data as $f=>$r)
        {
            array_push($r1,$f."="."'".$r."'");
        }

        $key = array_keys($tabel_id);
        $key = $key[0];
        $id = $tabel_id[$key];

        $row = implode(',',$r1);

        $query = $connection->query("UPDATE {$tabel} SET {$row} WHERE {$key}='{$id}'");

        return $query;
    }

    public static function delete($connection, $tabel_id,$table)
    {
        $key = array_keys($tabel_id);
        $key = $key[0];
        $id = $tabel_id[$key];

        $query = $connection->query("DELETE FROM {$table} WHERE {$key}='{$id}'");

        return $query;		
    }

}