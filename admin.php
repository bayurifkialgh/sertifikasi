<?php 
    $title = 'Admin';
    include 'partials/head.php';
    include 'helper/currency.php';
    include 'utils/DB.php';

    // New DB
    $db = DB::connect();

    // Call barang and penerbit
    include 'utils/barang.php';
    include 'utils/penerbit.php';

    $db->close();
?>
<body>
    <?php include 'partials/navbar.php' ?>

    <div class="mx-2 mt-5">
        <div class="row">

            <!-- Data barang -->
            <?php include 'admin-barang.php' ?>

            <!-- Data penerbit -->
            <?php include 'admin-penerbit.php' ?>
        </div>
        
    </div>

    <?php include 'partials/scripts.php' ?>   
</body>

