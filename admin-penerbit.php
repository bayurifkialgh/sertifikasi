<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            <h5>
                Data Penerbit
            </h5>
        </div>
        <div class="card-body">
            <form class="row mb-3" action="admin.php">
                <div class="col-md-6">
                    <input type="text" class="form-control" name="cariPenerbit" value="<?= $cariPenerbit ?? '' ?>">
                </div>
                <div class="col-md-6">
                    <div class="float-right">
                        <a href="admin.php" class="btn btn-warning">
                            Reset
                        </a>
                        <button class="btn btn-success" type="submit">
                            Cari
                        </button>
                    </div>
                </div>
            </form>
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="float-right">
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-create-penerbit">
                            Tambah
                        </button>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <td>Kode</td>
                            <td>Nama</td>
                            <td>Alamat</td>
                            <td>Kota</td>
                            <td>Telepon</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($getPenerbit as $gp) : ?>
                            <tr>
                                <td><?= $gp['id']; ?></td>
                                <td><?= $gp['nama']; ?></td>
                                <td><?= $gp['alamat']; ?></td>
                                <td><?= $gp['kota']; ?></td>
                                <td><?= $gp['telepon']; ?></td>
                                <td>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-update-penerbit<?= $gp['id'] ?>">
                                        Edit
                                    </button>
                                    <a href="admin.php?id=<?= $gp['id'] ?>&actionPenerbit=deletePenerbit" onclick="return confirm('Yakin?')"
                                        class="btn btn-danger btn-sm">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <?= $getPenerbit->num_rows == 0 ? '<tr><td colspan="100" class="text-center">Tidak ada data</td></tr>' : '' ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-create-penerbit" tabindex="-1" role="dialog" aria-labelledby="modal-create-label-penerbit"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="admin.php?actionPenerbit=createPenerbit" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-create-label-penerbit">Buat Penerbit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" required> 
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Alamat</label>
                            <input type="text" class="form-control" name="alamat" required>
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Kota</label>
                            <input type="text" class="form-control" name="kota" required>
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Telepon</label>
                            <input type="number" class="form-control" name="telepon" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php foreach($getPenerbit as $upt) : ?>
<div class="modal fade" id="modal-update-penerbit<?= $upt['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modal-update-label-penerbit<?= $upt['id'] ?>"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="admin.php?actionPenerbit=updatePenerbit&id=<?= $upt['id'] ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-update-label-penerbit<?= $upt['id'] ?>">Update Penerbit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" required value="<?= $upt['nama'] ?>"> 
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Alamat</label>
                            <input type="text" class="form-control" name="alamat" required value="<?= $upt['alamat'] ?>">
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Kota</label>
                            <input type="text" class="form-control" name="kota" required value="<?= $upt['kota'] ?>">
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Telepon</label>
                            <input type="number" class="form-control" name="telepon" required value="<?= $upt['telepon'] ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php endforeach; ?>