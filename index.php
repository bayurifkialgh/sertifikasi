<?php 
    $title = 'Home';
    include 'partials/head.php';
    include 'helper/currency.php';
    include 'utils/DB.php';

    // New DB
    $db = DB::connect();

    // Call barang
    include 'utils/barang.php';

    $db->close();
?>
<body>
    <?php include 'partials/navbar.php' ?>

    <div class="container mt-5">
        <div class="card">
            <div class="card-header">
                <h5>
                    <?php echo $title ?>
                </h5>
            </div>
            <div class="card-body">
                <form class="row mb-3" action="index.php">
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="cariBarang" value="<?= $cariBarang ?? '' ?>">
                    </div>
                    <div class="col-md-6">
                        <div class="float-right">
                            <a href="index.php" class="btn btn-warning">
                                Reset
                            </a>
                            <button class="btn btn-success" type="submit">
                                Cari
                            </button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>Kode</td>
                                <td>Penerbit</td>
                                <td>Kategori</td>
                                <td>Nama</td>
                                <td>Harga</td>
                                <td>Stok</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($getBarang as $d) :?>
                                <tr>
                                    <td><?= $d['id'] ?></td>
                                    <td><?= $d['penerbit'] ?></td>
                                    <td><?= $d['kategori'] ?></td>
                                    <td><?= $d['nama'] ?></td>
                                    <td><?= rupiah($d['harga']) ?></td>
                                    <td><?= number_format($d['stok']) ?></td>
                                </tr>
                            <?php endforeach ?>
                            <?= $getBarang->num_rows == 0 ? '<tr><td colspan="5" class="text-center">Tidak ada data</td></tr>' : '' ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php include 'partials/scripts.php' ?>   
</body>

